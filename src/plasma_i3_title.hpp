/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <optional>

#include <QJsonObject>
#include <QList>
#include <QObject>
#include <QString>
#include <QVector>

#include <i3ipc.h>

#include "lvd/logger.hpp"

#include "plasma_i3_title_window.hpp"
#include "plasma_i3_title_wspace.hpp"

// ----------

namespace lvd::plasma::i3::title {

class PlasmaI3TitleWindow;
using PlasmaI3TitleWindows = QVector<PlasmaI3TitleWindow>;

class PlasmaI3TitleWspace;
using PlasmaI3TitleWspaces = QVector<PlasmaI3TitleWspace>;

class PlasmaI3TitleModesModel;
class PlasmaI3TitlePagerModel;
class PlasmaI3TitleTitleModel;

// ----------

class PlasmaI3Title : public QObject {
  Q_OBJECT LVD_LOGGER

  using Window = PlasmaI3TitleWindow;
  using Windows = PlasmaI3TitleWindows;

  using Wspace = PlasmaI3TitleWspace;
  using Wspaces = PlasmaI3TitleWspaces;

  using ModesModel = PlasmaI3TitleModesModel;
  friend ModesModel;
  ModesModel* modes_model_ = nullptr;

  using PagerModel = PlasmaI3TitlePagerModel;
  friend PagerModel;
  PagerModel* pager_model_ = nullptr;

  using TitleModel = PlasmaI3TitleTitleModel;
  friend TitleModel;
  TitleModel* title_model_ = nullptr;

 public:
  PlasmaI3Title(QObject* parent = nullptr);

  Q_INVOKABLE PlasmaI3TitleModesModel* modes_model() const {
    return modes_model_;
  }

  Q_INVOKABLE PlasmaI3TitlePagerModel* pager_model() const {
    return pager_model_;
  }

  Q_INVOKABLE PlasmaI3TitleTitleModel* title_model() const {
    return title_model_;
  }

 public slots:
  void execute();

 private:
  void subscribe_i3mode();

  void establish_window();
  void subscribe_window();

  void establish_wspace();
  void subscribe_wspace();

 private:
  void  add_workspace (const Wspace & wspace );
  void  del_workspace (const Wspace & wspace );
  void  upd_workspace (const Wspace & wspace );

  void oust_workspaces(const Wspaces& wspaces);
  void sort_workspaces();

 private:
  void update_output(const QString& output);

  void update_i3mode(const QString& i3mode);
  void update_window(const Window & window);

 private:
  std::optional<Window> search_leaf(const QJsonObject& basis);
  std::optional<Window> search_tree(const QJsonObject& basis);

 private slots:
  void on_i3mode_event(const QString            i3mode,
                       bool                     ipango);

  void on_window_state(const i3ipc::DataObject& window_roster);
  void on_window_event(i3ipc::WindowChange      window_change,
                       const QJsonObject&       window_object);

  void on_wspace_state(const i3ipc::DataArray & wspace_roster);
  void on_wspace_event(i3ipc::WorkspaceChange   wspace_change,
                       const QJsonObject&       wspace_object,
                       const QJsonObject&       wspace_former);

  void on_socket_connected();
  void on_socket_disconnected();

 private:
  i3ipc* i3ipc_ = nullptr;

  QString output_;
  QString i3mode_;

  Window  window_;
  Wspaces wspaces_;
};

}  // namespace lvd::plasma::i3::title
