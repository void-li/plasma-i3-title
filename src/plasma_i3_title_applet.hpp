/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>
#include <QVariantList>

#include <Plasma/Applet>  // IWYU pragma: keep
// IWYU pragma: no_include <plasma/applet.h>

#include "lvd/logger.hpp"

#include "plasma_i3_title.hpp"

// ----------

namespace lvd::plasma::i3::title {

class PlasmaI3TitleApplet : public Plasma::Applet {
  Q_OBJECT LVD_LOGGER

 public:
  PlasmaI3TitleApplet(QObject* parent, const QVariantList& args);

  Q_INVOKABLE PlasmaI3Title* nativeInterface() const {
    return plasma_i3_title_;
  }

 private:
  PlasmaI3Title* plasma_i3_title_ = nullptr;
};

}  // namespace lvd::plasma::i3::title
