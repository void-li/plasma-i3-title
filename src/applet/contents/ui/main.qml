/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */

import QtQuick 2.13
import QtQuick.Window 2.13
import QtQuick.Layouts 1.13

import org.kde.plasma.core 2.0
import org.kde.plasma.plasmoid 2.0

// ----------

Item {
  id: main

  Plasmoid.compactRepresentation: Item {
    id: base

    Layout.fillHeight: true
    Layout.fillWidth : true

    Item {
      id: item

      height: parent.height
      width:  parent.width

      property int lx: -1
      property int rx: -1

      function resize() {
        var lx = base.mapToGlobal(         0,           0).x - Screen.virtualX
        var ld = +(Screen.width / 2 - lx)

        var rx = base.mapToGlobal(base.width, base.height).x - Screen.virtualX
        var rd = -(Screen.width / 2 - rx)

        var ed = Math.min(ld, rd)
        item.width = ed * 2

        var lo = ld - ed
        item.x     = lo

        if (   lx !== item.lx
            || rx !== item.rx) {
          item.lx = lx
          item.rx = rx

          item_timer.start()
        }
      }

      Text {
        anchors.fill: parent

        horizontalAlignment: Text.AlignHCenter
        verticalAlignment:   Text.AlignVCenter

        elide: Text.ElideRight

        text: plasmoid.nativeInterface.nativeInterface().title_model().title
      }

      Timer {
        id: item_timer

        interval: 1
        running: false
        repeat: false

        onTriggered: {
          item.resize()
        }
      }

      Component.onCompleted: {
        item_timer.start()
      }
    }

    onHeightChanged: {
      item_timer.start()
    }
    onWidthChanged: {
      item_timer.start()
    }

    Component.onCompleted: {
      plasmoid.nativeInterface.nativeInterface().title_model().output = Qt.binding(function() {
        return Screen.name
      })
    }
  }
}
