/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include "plasma_i3_title_wspace.hpp"
#include "lvd/core.hpp"  // IWYU pragma: keep

#include <QJsonValue>

// ----------

namespace lvd::plasma::i3::title {

PlasmaI3TitleWspace PlasmaI3TitleWspace::create(
    const QJsonObject& qjsonobject,
    const bool         allowurgent) {
  PlasmaI3TitleWspace plasma_i3_title_wspace;

  auto name_v = qjsonobject["name"];
  if (name_v.isString()) {
    QString name = name_v.toString();

    int index = name.indexOf(':');
    if (index > -1) {
      plasma_i3_title_wspace.        name_ = name;
      plasma_i3_title_wspace.display_name_ = name.mid(1+ index);
      plasma_i3_title_wspace.sorting_name_ = name.mid(0, index);
    } else {
      plasma_i3_title_wspace.        name_ = name;
      plasma_i3_title_wspace.display_name_ = name;
      plasma_i3_title_wspace.sorting_name_ = name;
    }
  }

  auto iden_v = qjsonobject[ "id" ];
  if (iden_v.isDouble()) {
    plasma_i3_title_wspace.iden_ = static_cast<quint64>(iden_v.toDouble());
  }

  auto focused_v = qjsonobject["focused"];
  if (focused_v.isBool()) {
    plasma_i3_title_wspace.focused_ = focused_v.toBool();
  }

  auto visible_v = qjsonobject["visible"];
  if (visible_v.isBool()) {
    plasma_i3_title_wspace.visible_ = visible_v.toBool();
  }

  auto urgent_v  = qjsonobject["urgent" ];
  if (urgent_v .isBool() && allowurgent) {
    plasma_i3_title_wspace.urgent_  = urgent_v .toBool();
  }

  auto output_v  = qjsonobject["output" ];
  if (output_v .isString()) {
    plasma_i3_title_wspace.output_  = output_v .toString();
  }

  return plasma_i3_title_wspace;
}

// ----------

bool PlasmaI3TitleWspace::identical(
    const PlasmaI3TitleWspace& plasma_i3_title_wspace) const {
  if (iden_ == plasma_i3_title_wspace.iden_ && iden_) {
    return true;
  }

  if (name_ == plasma_i3_title_wspace.name_) {
    return true;
  }

  return false;
}

void PlasmaI3TitleWspace::integrate(
    const PlasmaI3TitleWspace& plasma_i3_title_wspace) {
          name_ = plasma_i3_title_wspace.        name_;
  display_name_ = plasma_i3_title_wspace.display_name_;
  sorting_name_ = plasma_i3_title_wspace.sorting_name_;

  if (plasma_i3_title_wspace.iden_   ) {
    iden_    = plasma_i3_title_wspace.iden_;
  }

  // i3 sends invalid ipc messages
  if (plasma_i3_title_wspace.focused_ && !focused_.value_or(false)) {
    focused_ = plasma_i3_title_wspace.focused_;
  }

  if (plasma_i3_title_wspace.visible_) {
    visible_ = plasma_i3_title_wspace.visible_;
  }

  if (plasma_i3_title_wspace.urgent_ ) {
    urgent_  = plasma_i3_title_wspace.urgent_ ;
  }

  output_  = plasma_i3_title_wspace.output_ ;
}

}  // namespace lvd::plasma::i3::title
