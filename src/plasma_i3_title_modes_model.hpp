/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QObject>
#include <QString>

#include "lvd/logger.hpp"

#include "plasma_i3_title.hpp"

// ----------

namespace lvd::plasma::i3::title {

class PlasmaI3TitleModesModel : public QObject {
  Q_OBJECT LVD_LOGGER_LIKE(PlasmaI3Title)

  Q_PROPERTY(QString output READ output WRITE set_output NOTIFY output_changed)

  Q_PROPERTY(QString modes READ modes NOTIFY modes_changed)

 public:
  PlasmaI3TitleModesModel(PlasmaI3Title* parent)
      : QObject(parent),
        parent_(parent) {}

 public:
  QString output() const {
    return parent_->output_;
  }
  void set_output(const QString& output) {
    parent_->update_output(output);
  }

  QString modes() const {
    return parent_->i3mode_;
  }

 signals:
  void output_changed(const QString& output);
  void modes_changed(const QString& modes);

 private:
  PlasmaI3Title* parent_ = nullptr;
  friend PlasmaI3Title;
};

}  // namespace lvd::plasma::i3::title
