add_subdirectory(lvd-core)

# ----------

include(ExternalProject)

# qi3pc

set(QI3PC_INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/qi3pc_ext")
file(MAKE_DIRECTORY "${QI3PC_INSTALL_DIR}")

set(QI3PC_INCLUDE_DIR "${QI3PC_INSTALL_DIR}/${CMAKE_INSTALL_INCLUDEDIR}")
file(MAKE_DIRECTORY "${QI3PC_INCLUDE_DIR}")

set(QI3PC_LIBRARY_DIR "${QI3PC_INSTALL_DIR}/${CMAKE_INSTALL_LIBDIR}")
file(MAKE_DIRECTORY "${QI3PC_LIBRARY_DIR}")

ExternalProject_Add(qi3pc_ext
  GIT_REPOSITORY "https://gitlab.com/hantz/qi3pc.git"
  GIT_TAG "origin/master"

  GIT_SHALLOW ON
  UPDATE_DISCONNECTED ON

  BUILD_BYPRODUCTS "${QI3PC_LIBRARY_DIR}/libqi3pc.a"

  PATCH_COMMAND bash -c "\
    git checkout -- . &&\
    git clean -f -- . &&\
    patch -Np1 -r- < <(cat ${CMAKE_CURRENT_SOURCE_DIR}/qi3pc-qmk.patch)&&\
    patch -Np1 -r- < <(cat ${CMAKE_CURRENT_SOURCE_DIR}/qi3pc-src.patch)&&\
    true\
  "

  CONFIGURE_COMMAND
    INC_DIR=${QI3PC_INCLUDE_DIR}
    LIB_DIR=${QI3PC_LIBRARY_DIR}
    qmake ../qi3pc_ext

  INSTALL_COMMAND DESTDIR= make install
)

add_library(          qi3pc_3rd STATIC IMPORTED GLOBAL)
set_target_properties(qi3pc_3rd PROPERTIES
  IMPORTED_LOCATION
    "${QI3PC_LIBRARY_DIR}/libqi3pc.a"
  INTERFACE_INCLUDE_DIRECTORIES
    "${QI3PC_INCLUDE_DIR}"
)

add_dependencies(qi3pc_3rd qi3pc_ext)
