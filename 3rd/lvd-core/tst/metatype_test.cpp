/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QMetaType>
#include <QObject>
#include <QString>

#include "metatype.hpp"

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class MetatypeTestType {};

Q_DECLARE_METATYPE(MetatypeTestType);
L_DECLARE_METATYPE(MetatypeTestType, MetatypeTestType);

// ----------

class MetatypeTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void metatype() {
    QVERIFY(QMetaType::type("MetatypeTestType") == QMetaType::UnknownType);
    QVERIFY(QMetaType::type("MetatypeTestType") == QMetaType::UnknownType);

    Metatype::metatype();

    QVERIFY(QMetaType::type("MetatypeTestType") != QMetaType::UnknownType);
    QVERIFY(QMetaType::type("MetatypeTestType") != QMetaType::UnknownType);
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(MetatypeTest)
#include "metatype_test.moc"  // IWYU pragma: keep
