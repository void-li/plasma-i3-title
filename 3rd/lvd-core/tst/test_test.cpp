/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#include <QtTest>
#include "core.hpp"  // IWYU pragma: keep

#include <QObject>
#include <QString>

#include "test.hpp"
using namespace lvd;
using namespace lvd::test;

// ----------

class TestTest : public Test {
  Q_OBJECT

 private slots:
  void test_ok() {
    QVERIFY(true);
  }

  // ----------

  void execution() {
    int ret;

    ret = execute("true" );
    QCOMPARE(ret, 0);

    ret = execute("false");
    QCOMPARE(ret, 1);
  }

  void execution_stdout() {
    int ret;

    ret = execute("bash", "-c", "echo Mera Luna >&1");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_, "Mera Luna");
    QCOMPARE(execute_stderr_, QString());
  }

  void execution_stderr() {
    int ret;

    ret = execute("bash", "-c", "echo Mera Luna >&2");
    QCOMPARE(ret, 0);

    QCOMPARE(execute_stdout_, QString());
    QCOMPARE(execute_stderr_, "Mera Luna");
  }

  // ----------

 private slots:
  void init() {
    Test::init();
  }

  void initTestCase() {
    Test::initTestCase();
  }

  void cleanup() {
    LVD_FINALLY {
      Test::cleanup();
    };
  }

  void cleanupTestCase() {
    LVD_FINALLY {
      Test::cleanupTestCase();
    };
  }
};

LVD_TEST_MAIN(TestTest, TestTest)
#include "test_test.moc"  // IWYU pragma: keep
