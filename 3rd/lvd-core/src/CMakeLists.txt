find_package(Qt5 REQUIRED COMPONENTS Core Test)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

if(LVD_QML_APPLICATION)
  set(LVD_QML_APPLICATION_IMPL "42")
  find_package(Qt5 REQUIRED COMPONENTS Gui)
endif()

if(LVD_GUI_APPLICATION)
  set(LVD_GUI_APPLICATION_IMPL "42")
  find_package(Qt5 REQUIRED COMPONENTS Widgets)
endif()

# ----------

if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp")
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
else()
  configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/config.cpp.in"
    "${CMAKE_CURRENT_BINARY_DIR}/config.cpp" @ONLY
  )
endif()

# ----------

if(NOT TARGET lvd-core)
  add_library(lvd-core STATIC EXCLUDE_FROM_ALL
    access.cpp
    access.hpp
    application.cpp
    application.hpp
    config.cpp.in "${CMAKE_CURRENT_BINARY_DIR}/config.cpp"
    config.hpp
    core.cpp
    core.hpp
    core_enum.cpp
    core_enum.hpp
    core_eval.cpp
    core_eval.hpp
    core_task.cpp
    core_task.hpp
    core_task.hxx
    filewatcher.cpp
    filewatcher.hpp
    logger.cpp
    logger.hpp
    metatype.cpp
    metatype.hpp
    settings.cpp
    settings.hpp
    shield.cpp
    shield.hpp
    signal.cpp
    signal.hpp
    string.cpp
    string.hpp
  )

  target_link_libraries(lvd-core
    Qt5::Core
    $<$<BOOL:${LVD_QML_APPLICATION_IMPL}>:Qt5::Gui>
    $<$<BOOL:${LVD_GUI_APPLICATION_IMPL}>:Qt5::Widgets>
  )

  target_include_directories(lvd-core INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/include
  )
endif()

# ----------

if(NOT TARGET lvd-core-test)
  add_library(lvd-core-test STATIC EXCLUDE_FROM_ALL
    test.cpp
    test.hpp
  )

  target_link_libraries(lvd-core-test
    lvd-core
    Qt5::Core
    Qt5::Test
  )

  target_include_directories(lvd-core-test INTERFACE
    ${CMAKE_CURRENT_SOURCE_DIR}/include
  )
endif()
