/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

// ----------

namespace lvd {

#define LVD_EVAL_IMPL_A(...) __VA_ARGS__
#define LVD_EVAL_IMPL_B(...) \
  LVD_EVAL_IMPL_A(LVD_EVAL_IMPL_A(LVD_EVAL_IMPL_A(__VA_ARGS__)))
#define LVD_EVAL_IMPL_C(...) \
  LVD_EVAL_IMPL_B(LVD_EVAL_IMPL_B(LVD_EVAL_IMPL_B(__VA_ARGS__)))
#define LVD_EVAL_IMPL_D(...) \
  LVD_EVAL_IMPL_C(LVD_EVAL_IMPL_C(LVD_EVAL_IMPL_C(__VA_ARGS__)))
#define LVD_EVAL_IMPL_E(...) \
  LVD_EVAL_IMPL_D(LVD_EVAL_IMPL_D(LVD_EVAL_IMPL_D(__VA_ARGS__)))
#define LVD_EVAL_IMPL_F(...) \
  LVD_EVAL_IMPL_E(LVD_EVAL_IMPL_E(LVD_EVAL_IMPL_E(__VA_ARGS__)))

#define LVD_EVAL_VOID
#define LVD_EVAL_VOID_A(...)
#define LVD_EVAL_VOID_B() 0, LVD_EVAL_VOID_A

#define LVD_EVAL_SEP ,
#define LVD_EVAL_SUM +

#define LVD_EVAL_MAP_K(X, N, ...) N LVD_EVAL_VOID
#define LVD_EVAL_MAP_J(X, N) LVD_EVAL_MAP_K(X, LVD_EVAL_SEP N, 0)
#define LVD_EVAL_MAP_I(X, N) LVD_EVAL_MAP_J(LVD_EVAL_VOID_B X, N)

#define LVD_EVAL_MAP_A(F, X, N, ...) \
  F(X) LVD_EVAL_MAP_I(N, LVD_EVAL_MAP_B)(F, N, __VA_ARGS__)
#define LVD_EVAL_MAP_B(F, X, N, ...) \
  F(X) LVD_EVAL_MAP_I(N, LVD_EVAL_MAP_A)(F, N, __VA_ARGS__)

#define LVD_EVAL(F, ...) \
  LVD_EVAL_IMPL_F(LVD_EVAL_MAP_A(F, __VA_ARGS__, (), 0))

}  // namespace lvd
