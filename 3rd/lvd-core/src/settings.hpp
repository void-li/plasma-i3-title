/**
 * Copyright © 2021 Luca Lovisa <opensource@void.li>
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/ for more details.
 * SPDX-License-Identifier: WTFPL
 */
#pragma once

#include <QtGlobal>

#include <QObject>
#include <QSettings>
#include <QString>

// ----------

namespace lvd {

class Settings : public QSettings {
  Q_OBJECT

 public:
  Settings(const QString& organization,
           const QString& application = QString(),
           QObject*       parent = nullptr);

  Settings(Scope          scope,
           const QString& organization,
           const QString& application = QString(),
           QObject*       parent = nullptr);

  Settings(Format         format,
           Scope          scope,
           const QString& organization,
           const QString& application = QString(),
           QObject*       parent = nullptr);

  Settings(const QString& fileName,
           Format         format,
           QObject*       parent = nullptr);

#if QT_VERSION >= QT_VERSION_CHECK(5, 13, 0)
  Settings(Scope          scope,
           QObject*       parent = nullptr);
#endif

  Settings(QObject*       parent = nullptr);

 public:
  static QString system_config();
  static QString client_config();
  static QString custom_config();
};

}  // namespace lvd
